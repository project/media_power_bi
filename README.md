## INTRODUCTION

This module allows the creation of Media assets from Microsoft Power BI. This allows you to manage create visualizations within Power BI and then embed them within Drupal anywhere Media allows, including fields and WYSIWYG.

## REQUIREMENTS

Works with Drupal 9 and 10.

## INSTALLATION

Install as you would normally install a contributed Drupal module.
See: https://www.drupal.org/node/895232 for further information.

## CONFIGURATION

- Create a new Media Type and select "Media Power BI".

## MAINTAINERS

Current maintainers for Drupal 9/10:

- Betty Tran (minirobot) - https://www.drupal.org/u/minirobot
