<?php

declare(strict_types=1);

namespace Drupal\media_power_bi\Plugin\media\Source;

use Drupal\media\MediaSourceBase;
use Drupal\media\MediaSourceFieldConstraintsInterface;

/**
 * A media source plugin for Power BI media embeds.
 *
 * @MediaSource(
 *   id = "media_power_bi",
 *   label = @Translation("Media Power BI"),
 *   description = @Translation("A media source plugin for Power BI."),
 *   allowed_field_types = {"string_long"},
 *   default_thumbnail_filename = "generic.png",
 *   forms = {
 *     "media_library_add" = "Drupal\media_power_bi\Form\MediaPowerBiMediaForm"
 *   }
 * )
 */
class MediaPowerBiSource extends MediaSourceBase implements MediaSourceFieldConstraintsInterface {

  /**
   * {@inheritdoc}
   */
  public function getMetadataAttributes() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getSourceFieldConstraints() {
    return [
      'media_power_bi' => [],
    ];
  }

}
