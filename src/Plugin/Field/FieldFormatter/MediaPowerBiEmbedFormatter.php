<?php

declare(strict_types=1);

namespace Drupal\media_power_bi\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'media_power_bi' formatter.
 *
 * @FieldFormatter(
 *   id = "media_power_bi",
 *   label = @Translation("Media Power BI"),
 *   field_types = {
 *     "string_long"
 *   }
 * )
 */
class MediaPowerBiEmbedFormatter extends FormatterBase {

  const POWER_BI_DOMAIN = 'https://app.powerbi.us';
  const POWER_BI_GOV_DOMAIN = 'https://app.powerbigov.us';

  /**
   * Gets the regex pattern for matching Power BI domain urls.
   *
   * @return string
   *   Returns a string containing the regex pattern.
   */
  public static function getUrlRegexPattern() {
    return '/^' . preg_quote(self::POWER_BI_DOMAIN, '/') . '|' . preg_quote(self::POWER_BI_GOV_DOMAIN, '/') . '\/view\?r=(\w+)/';
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    foreach ($items as $delta => $item) {
      /** @var \Drupal\Core\Field\FieldItemInterface $item */
      if ($item->isEmpty()) {
        continue;
      }

      $matches = [];
      $pattern = static::getUrlRegexPattern();
      preg_match_all($pattern, $item->value, $matches);

      if (empty($matches[0][0])) {
        continue;
      }

      $elements[$delta] = [
        '#theme' => 'media_power_bi',
        '#url' => $matches[0][0],
        '#title' => $item->getParent()->getParent()->get('name')->value,
        '#width' => $this->getSetting('width'),
        '#height' => $this->getSetting('height'),
      ];
    }
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'width' => '100%',
      'height' => '900px',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return parent::settingsForm($form, $form_state) + [
      'width' => [
        '#type' => 'textfield',
        '#title' => $this->t('Width'),
        '#default_value' => $this->getSetting('width'),
        '#size' => 5,
        '#maxlength' => 10,
        '#description' => $this->t('Valid css unit e.g. 500px, 100%, etc.'),
      ],
      'height' => [
        '#type' => 'textfield',
        '#title' => $this->t('Height'),
        '#default_value' => $this->getSetting('height'),
        '#size' => 5,
        '#maxlength' => 10,
        '#description' => $this->t('Valid css unit e.g. 500px, 100%, etc.'),
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    $summary[] = $this->t('Iframe size: %width , %height.', [
      '%width' => $this->getSetting('width'),
      '%height' => $this->getSetting('height'),
    ]);
    return $summary;
  }

}
