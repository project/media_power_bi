<?php

declare(strict_types=1);

namespace Drupal\media_power_bi\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Checks if a value represents a valid remote resource URL.
 *
 * @Constraint(
 *   id = "media_power_bi",
 *   label = @Translation("Media Power BI resource", context = "Validation"),
 *   type = {"string"}
 * )
 */
class MediaPowerBiConstraint extends Constraint {

  /**
   * The error message if the URL is empty.
   *
   * @var string
   */
  public $emptyUrlMessage = 'The URL cannot be empty.';

  /**
   * The error message if the URL does not match.
   *
   * @var string
   */
  public $invalidUrlMessage = 'This does not appear to contain a valid Power BI embed. Double check against the code copied from the share or embed dialog in Power BI and try again.';

}
