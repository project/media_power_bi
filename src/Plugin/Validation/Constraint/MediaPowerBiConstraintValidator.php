<?php

declare(strict_types=1);

namespace Drupal\media_power_bi\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates Power BI embed code and URIs.
 */
class MediaPowerBiConstraintValidator extends ConstraintValidator {

  const POWER_BI_DOMAIN = 'https://app.powerbi.us';

  const POWER_BI_GOV_DOMAIN = 'https://app.powerbigov.us';

  /**
   * {@inheritdoc}
   */
  public function validate($value, Constraint $constraint) {
    /** @var \Drupal\media\MediaInterface $media */
    $media = $value->getEntity();
    $source = $media->getSource();

    $url = $source->getSourceFieldValue($media);
    $url = trim($url, "/");
    // The URL may be NULL if the source field is empty, which is invalid input.
    if (empty($url)) {
      $this->context->addViolation($constraint->emptyUrlMessage);
      return;
    }

    $pattern = self::getUrlRegexPattern();
    if (!preg_match($pattern, $url)) {
      $this->context->addViolation($constraint->invalidUrlMessage);
    }
  }

  /**
   * Gets the regex pattern for matching Power BI domain urls.
   *
   * @return string
   *   Returns a string containing the regex pattern.
   */
  public static function getUrlRegexPattern() {
    return '/^' . preg_quote(self::POWER_BI_DOMAIN, '/') . '|' . preg_quote(self::POWER_BI_GOV_DOMAIN, '/') . '\/view\?r=(\w+)/';
  }

}
